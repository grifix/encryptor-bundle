<?php

declare(strict_types=1);

namespace Grifix\EncryptorBundle\Tests;

use Grifix\Encryptor\Encryptor\EncryptorInterface;

final class EncryptorWrapper
{
    public function __construct(private readonly EncryptorInterface $encryptor)
    {
    }

    public function getEncryptor(): EncryptorInterface
    {
        return $this->encryptor;
    }
}
