<?php

declare(strict_types=1);

namespace Grifix\EncryptorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class GrifixEncryptorBundle extends Bundle
{
}
