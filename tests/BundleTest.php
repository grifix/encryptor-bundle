<?php

declare(strict_types=1);

namespace Grifix\EncryptorBundle\Tests;

use Grifix\EncryptorBundle\GenerateKeyCommand;
use Grifix\EncryptorBundle\GrifixEncryptorBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;

final class BundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    /**
     * @param mixed[] $options
     */
    protected static function createKernel(array $options = []): KernelInterface
    {
        /** @var TestKernel $kernel */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixEncryptorBundle::class);
        $kernel->addTestConfig(__DIR__ . '/test_config.yaml');

        return $kernel;
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();
        /** @var EncryptorWrapper $wrapper */
        $wrapper = $kernel->getContainer()->get(EncryptorWrapper::class);

        $string = 'secret';
        $encryptedString = $wrapper->getEncryptor()->encrypt($string);
        self::assertEquals(
            $string,
            $wrapper->getEncryptor()->decrypt($encryptedString),
        );
    }

    public function testItGeneratesKey(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        /** @var string $commandName */
        $commandName = GenerateKeyCommand::getDefaultName();
        $command = $application->find($commandName);
        $commandTester = new CommandTester($command);
        $commandTester->execute([
          'command' => $command->getName(),
        ]);
        self::assertNotEmpty($commandTester->getDisplay());
    }
}
