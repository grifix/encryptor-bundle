<?php

declare(strict_types=1);

namespace Grifix\EncryptorBundle;

use Grifix\Encryptor\KeyGenerator\KeyGeneratorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Service\Attribute\Required;

final class GenerateKeyCommand extends Command
{
    #[Required]
    public KeyGeneratorInterface $keyGenerator;

    protected static $defaultName = 'grifix:encryptor:generate';

    protected static $defaultDescription = 'Generates encryption key';

    protected function execute(
        InputInterface $input,
        OutputInterface $output,
    ): int {
        $output->writeln($this->keyGenerator->generateKey());
        return self::SUCCESS;
    }
}
